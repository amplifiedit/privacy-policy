**Amplified IT, LLC**

**Privacy Policy**

**_Effective Date: June 20, 2016_**

##**OUR COMMITMENT TO YOUR PRIVACY**

Amplified IT, LLC and its affiliates, including Amplified Labs (collectively referred to as "**Amplified IT**," "**we**," "**our**," or "**us**") understands that security and privacy of personal information is very important to the schools, school districts, and related entities and organizations as well as their administrators (each a "**School**") that we serve and their students, parents, teachers and staff as well as visitors to our websites (collectively, "**you**").  This Privacy Policy ("**Privacy Policy**") describes how we collect, use, share and retain your information through a variety of digital means.  We may collect your information through our websites ("**Websites**"), your use of our tools ("**Tools**"), or through the support, audit and other services we provide ("**Services**").   We  provide a suite of services and complementary tools that enable educational institutions, teachers and students to get the most out of their Google Apps for Education platform (the "**Google Platform**").  Our Websites allow us to gather information about current and potential customers as well as share information about our Services and Tools.

By accessing or using our Websites, Tools and Services on any computer, mobile phone, tablet or other device (collectively, "**Device**") you consent to the terms of this Privacy Policy. We may modify this Privacy Policy at any time as stated below. Your continued use of our Websites, Tools and Services constitutes your acceptance of this Privacy Policy and any updates. This Privacy Policy is incorporated into, and is subject to, the Terms of Use for the Websites, Tools and Services.

**THE GEOGRAPHIC LIMITS OF OUR SERVICES**

Owing to the legal requirements and liabilities of the General Data Protection Regulation (GDPR) in the European Union, as of May 25th, 2018 our services are no longer supported for customers in Europe.

European users who have installed our services are hereby advised to cease their use.

We remain committed to data-handling practices that are fully-compliant with laws in North America, and that abide the standards of care required by FERPA.


**INFORMATION WE COLLECT**

We collect information to provide you with our innovative Tools and Services and to operate our business. We collect Personal User Data, Tool or Service Specific Data, and Anonymous User Data  in support of these efforts.

1. "**Personal User Data**" is information that can be used, directly or indirectly, alone or together with other information, to identify you as the user of one of our Tools or Websites. This may include your first and last name, email address, telephone number, and location.

2. "**Tool or Service Specific Data**" includes any information provided by you as part of the functionality of a Tool or the provision of the Services. This may include personal information about you as the user, account username, password, and student information such as student name, email rosters, and class codes.

3. "**Anonymous User Data**" is information that is anonymous, aggregate, de-identified, or otherwise does not reveal your identity. Some examples include school identifiers, IP address, Internet service provider, browser and operating system, referring and exit pages and URLs, date and time spent on our Websites or using the Tools, amount of time spent on particular pages, what sections of the Websites you visit, the number of links you click while on one of our Websites, search terms, website traffic and related statistics, keywords and/or other data.

**HOW WE COLLECT AND USE YOUR INFORMATION**

1. **Services.** Depending on the nature of the engagement, Schools may provide us with information required to provide the Services. This may include Tool or Service Specific Data, Personal User Data of students or staff as well as access to account credentials, Devices or systems that contain Personal User Data and/or Tool or Service Specific Data. This information is provided and controlled by your child’s school, and is only used in providing our Services in a manner consistent with this Privacy Policy. We do not use student Personal User Data to target ads.

We may also collect Anonymous User Data from our Services. This information may be used to improve our Services or Tools and provide customers with additional information about our products and services.

2. **Tools.**  You will be required to provide your email address when you use our Tools. We do not collect any other Personal User Data through your use of our Tools unless you are specifically prompted for participation within a Tool’s dashboard where we may collect Tool and Service Specific Data. You will be provided with an ongoing opt-out option when prompted for any additional information. Our Tools are explicitly designed not to prompt for or collect Personal User Data from student users. The Tool and Service Specific Data collected will be used for the exclusive purpose of providing you with access to paid features of the Tools.

We may collect Anonymous User Data such as the domain (e.g. "myschool.org") portion of your email address or other activity markers that allow us to analyze and report on aggregate user behavior. We use this information to improve our Tools, report usage to existing domain customers, to maintain license subscriptions, and for contacting schools that may be interested in our Services or Tools.

3. **Tools with Access to Google Services.** During installation, our Tools may ask either you or the domain administrator for the School to authorize us to take actions on your behalf within the services provided by the Google Platform. This authorization process utilizes [OAuth2.0](http://oauth.net/2/) protocols and provides our Tools with a token that allows us limited access and the ability to take specific actions within the Google Platform ("**Google Services**").

These Google Services typically involve sending, storing, and sharing information with others, and may include Personal User Data.  Your relationship with Google, Inc. with respect to the Google Services is defined under the terms of use and data privacy policies of Google, Inc.

Unless explicitly initiated by you, no information from within the Google Services is ever shared with us, stored by us in your Google account in a manner that is publicly accessible, or held at rest in a data storage location that is not directly under your control via your Google account.

4. **Websites.** We may collect Personal User Data from visitors on our Websites via web forms. This may include contact information, geographic location, information about the nature of pre-sales inquiries, product feedback, and support requests. Visitors volunteer the information they wish to share. We gather Anonymous User Data from your use of our Websites including browser type, time on page and click behavior. We use this information to better serve our current and potential customers, to identify future customers and to make improvements to our Services, Tools and Websites.

We also use cookies to compile aggregate data about traffic on our Websites so that we can offer better experiences and tools in the future. Cookies are small text files that help us to personalize the content of our Websites. These cookies do not contain and transmit Personal User Data. You can choose to disable and/or delete all cookies via your browser settings. However, if you disable or delete cookies, some features of our Websites may not function properly. We do not behaviorally target advertising.

**HOW WE SHARE YOUR INFORMATION**

We will only disclose your Personal User Data or Tool and Service Specific Data with your knowledge and as stated in this Privacy Policy or as otherwise indicated at the time of collection. We will only share your Personal User Data or Tool and Service Specific Data with third parties in certain limited instances, namely with your consent, or as is necessary to complete a transaction, or provide the Services or Tools.

We may disclose your Personal User Data or Tool and Service Specific Data as described in this Privacy Policy and in the following ways:

1. **Student Data.**  Student Personal User Data is not shared or disclosed except as authorized and directed by your child’s School. A School may instruct and authorize us to share student Personal User Data with third-party tools or applications it has approved for use by students and/or staff and for which it has obtained parental consent. This may include third-party tools or applications that allow schools to (i) monitor the online activities of students and staff, (ii) interact with parents, students and teachers, or (iii) enhance the education of their students. We may resell or support some of these third-party tools or applications subject to the request and authorization of a School. The practices of the third-party tools or applications will be governed by their privacy policy and terms of use. We strongly encourage you to review such policies prior to sharing any Personal User Data.

2. **Services and Tools.** In order to enable us to provide the Services we may provide Personal User Data or Anonymous User Data to certain third-party business partners whose products are complementary to our Services. This may include information related to the School and its IT liaison.

3. **Advertising.** We do not share your Personal User Data with any third-party for advertising purposes.

4. **Service Providers.** We may share Personal User Data or Tool and Service Specific Data with service providers who provide us with technology services such as web hosting, payment processing and analytics services. Personal User Data and Tool and Service Specific Data is provided to service providers strictly for the purpose of carrying out their work for us. When student Personal User Data is required, we require service providers to implement the same commitments to safeguarding student Personal User Data that we do.

5. **As Required by Law or to Protect Rights.** We may be required, subject to applicable law, to disclose your Personal User Data or Tool and Service Specific Data if: (i) it is reasonably necessary to comply with legal process (such as a court order, subpoena, search warrant, etc.) or other legal requirements of any governmental authority or as otherwise required by law, (ii) such a disclosure would potentially mitigate our liability in an actual or potential lawsuit, (iii) it is necessary to protect our legal rights or property, (iv) it is necessary to protect the legal rights or property or physical security of others, or (v) for the prevention or detection of crime and such disclosure is lawful.

6. **Business Transfers.** If we ever merge with another company, or should we decide to sell, divest or transfer some part or all of our business, we expect that the Personal User Data, Tool and Service Specific Data and Anonymous User Data we have collected will be transferred along with our other business assets. We will not transfer your Personal User Data or Tool and Service Specific Data unless the purchaser or successor entity is subject to the commitments contained in this Privacy Policy for previously collected Personal User Data and Tool and Service Specific Data. In such case we will provide you with notice and an opportunity to opt-out of the transfer of Personal User Data and Tool and Service Specific Data.

**HOW WE PROTECT YOUR INFORMATION**

Amplified IT is committed to protecting the security and privacy of your Personal User Data and Tool and Service Specific Data including the Personal User Data of the students in the schools we serve. We comply with applicable laws and regulations, which govern or apply to our Tools and the provision of our Services.

1. **Information Security.** We maintain strict administrative, technical, and physical procedures to protect any Personal User Data and Tool and Service Specific Data that is stored on our servers against unauthorized access, theft and loss.  Access to Personal User Data and Tool and Service Specific Data is limited to those employees or contractors who require it to perform their job functions and are bound by strict contractual confidentiality obligations. We use industry-standard Secure Socket Layer (SSL) encryption technology, user/password credentials and two factor authentication to safeguard access to Personal User Data and Tool and Service Specific Data. Any data transmitted between a local Device and Google or our cloud servers is sent in https encrypted form through a secure website, secure file transfer, or through Google Drive / Spreadsheet infrastructure. Other security safeguards include but are not limited to data encryption, firewalls, physical access controls to building and files, and other industry-standard best practices.

2. **Your Responsibility.** Our Tools may contain features that, if used incorrectly, can result in the inappropriate sharing of sensitive information including Personal User Data within the Google Platform. It is solely your responsibility to share sensitive information appropriately when using our Tools.

**MANAGING YOUR PRIVACY SETTINGS AND ACCESS TO  INFORMATION**

1. **Student Personal User Data** Student Personal User Data is provided and controlled by your child’s School. Upon request, we will grant you reasonable access to Personal User Data that we have about your child for the purpose of reviewing and/or requesting changes. We will use commercially reasonable efforts to process such requests in a timely manner. You should be aware, however, that it is not always possible to completely remove or modify information in our databases.

Parents have the right to ask that the online records of their children under the age of 13 be removed from the storage of any online service provider. If you wish to request that Amplified IT remove your child’s records from our database, please contact the appropriate official at your child’s School and ask them to carry out this action in collaboration with our team. If you have any questions about reviewing, modifying, or deleting your child’s records, please contact the appropriate official at your child’s School directly.

2. **Opting-Out.** You may opt-out of allowing us to share your Personal User Data and Tool and Service Specific Data with third-party business partners for the purpose of providing products and services that are complementary to our Services. You may also elect to opt-out when prompted by our Tools to provide any additional information or to allow access tokens.

3. **Access Tokens for Google Services.**  If you have installed our Tools and enabled the access tokens for Google Services, you can revoke the access tokens at any time from your Google Account settings. In the case of domain-installed apps, access tokens can be revoked by the domain administrator for your child’s School.

4. **Deleting or Disabling Cookies.** You may be able to disallow cookies to be set on your browser. Please look for instructions on how to delete or disable cookies and other tracking/recording tools on your browser’s technical settings. You may not be able to delete or disable cookies on certain mobile devices and/or certain browsers. For more information about managing your cookie settings, please visit www.allaboutcookies.org. Please note that if you disable and do not accept cookies, some features and services may not be available to you.

5. **Manage Your Geolocation Services.** Most mobile Devices provide users with the ability to disable location services. Disabling location services will not allow us to collect your location data (by virtue of the location of your mobile Device) when you use our Tools. Most likely, these controls are located in the Device’s settings menu. If you have questions about how to disable your Device’s location services, we recommend you contact your mobile service carrier or your Device manufacturer.

**DATA RETENTION**

Except as set forth below, we may retain Personal User Data for the period of time required to provide the Services or the Tools. We may continue to retain Personal User Data for a commercially reasonable time following termination of the Services (i) for backup, archival, or audit purposes, (ii) to enforce our Terms of Use, or (iii) to comply with our legal obligations.  Access credentials associated with a School will be promptly deleted upon termination. We may retain and use Anonymous User Data indefinitely.

For Services that involve provisioning of student accounts or other Google resources, student Personal User Data may be retained for the period of time required for the student to have access to our Tools or for us to perform the Services. We do not sell student Personal User Data. Student Personal User Data will be deleted in accordance with the retention policy contained in the applicable terms of use unless we are otherwise instructed by your child’s School.

### **CHILDREN’S PRIVACY**

**(a)** **Services and Tools.** Schools may provide student Personal User Data, which may include "educational records" as defined by the Family Educational Rights and Privacy Act ("**FERPA**"), in connection with the provision of Services or use of our Tools.  Student Personal User Data is owned and controlled by the Schools at all times. Schools are solely responsible for compliance with the Children’s Online Privacy Protection Act of 1998 ("**COPPA**") and FERPA, including, but not limited to, obtaining parental consent concerning the collection of students’ Personal User Data used in connection with the provisioning and use of the Tools or as otherwise contained in this Privacy Policy. Schools are solely responsible for obtaining parental consent before allowing any User under the age of 13 to use any of the Tools.



**(b)** **Websites.** Except as contained in this Privacy Policy, we do not knowingly collect any Personal User Data from children who are under the age of 13 through our Websites. If you are under the age of 13, please do not provide any Personal User Data through our Websites. Please contact us if you believe we have inadvertently collected or been provided with Personal User Data of a child under the age of 13 without proper parental consents so that we may delete such data as soon as possible.

**LINKS TO THIRD-PARTY WEBSITES, APPLICATIONS AND SERVICES**

Our Websites and Tools may contain links to or from third-party websites, applications or services. Please note that this Privacy Policy does not apply to the practices of companies we do not own or control or to people we do not employ or manage. As such, we have no control over the privacy practices of third parties and are not responsible for the privacy policies of any third-party websites, applications or services. We recommend that you review the privacy policies and terms of use of any third-party prior to visiting its website, downloading its applications or authorizing the third-party to access Personal User Data through our Services.

**INTERNATIONAL USERS**

Our servers are located and our digital operations are conducted in the United States. Regardless of where you live, you consent to have your Personal User Data and any Tool and Service Specific Data transferred, processed and stored in the United States, and allow Amplified IT to use and collect your Personal User Data, Tool and Service Specific Data and Anonymous User Data in accordance with this Privacy Policy.

**HOW TO CONTACT US**

If you have any questions or would like to learn more about this Privacy Policy, please contact us at support@amplifiedit.com.

**CHANGES TO OUR PRIVACY POLICY**

We may modify this Privacy Policy from time to time so you should review this page periodically. [Click here](https://bitbucket.org/amplifiedit/privacy-policy) to see all changes to this Privacy Policy. If we make material changes to this Privacy Policy, we will provide our customers with notice at least 30 days prior to the changes taking effect so each customer has time to evaluate the new policy.  If a customer does not wish to be bound by the new policy, the primary administrative contact for the customer can request that we delete the School’s data from our systems.

